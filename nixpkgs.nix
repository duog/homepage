let
    rev = "2a652aea074d647fdbce817e3f94b7e056a0f5ae";
    src = fetchTarball {
      url = "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz";
      sha256 = "190d7vr41n5qf5lc8ifkpd4hm7a3vsnnrgmzzc5axak8khpr2b1l";
    };
in
import src

