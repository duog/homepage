---
author: bgamari
title: "GHC 8.10.3 released"
date: 2020-12-19
tags: release
---

The GHC team is happy to announce the release of GHC 8.10.3. Source
and binary distributions are available at the [usual place](https://downloads.haskell.org/ghc/8.10.3/).

GHC 8.10.3 fixes a number of issues in present in GHC 8.10.2 including:

 * Numerous stability improves on Windows

 * More robust support for architectures with weak memory ordering
   guarantees (e.g. modern ARM hardware).

 * GHC can now split dynamic objects to accomodate macOS' RPATH size
   limitation when building large projects (#14444)

 * Several correctness bugs in the new low-latency garbage collector

Note that at the moment we still require that macOS Catalina users
exempt the binary distribution from the notarization requirement by
running `xattr -cr .` on the unpacked tree before running `make install`.
This situation will hopefully be improved for GHC 9.0.1 with the
resolution of #17418.

