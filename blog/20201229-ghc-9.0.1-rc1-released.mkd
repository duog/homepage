---
author: bgamari
title: "GHC 9.0.1 release candidate 1 now available"
date: 2020-12-29
tags: release
---

The GHC team is very pleased to announce the availability of the first
release candidate of GHC 9.0.1 series. Source and binary distributions are
available at the [usual place](https://downloads.haskell.org/ghc/9.0.1-rc1/).

This release candidate comes quite a bit later than expected after
difficulty finding a performance neutral fix for a critical soundness
bug, #17760. See the [migration page][1] for details on the solution, particularly
if you are a library maintainer currently using the `touch#` primop or
`Foreign.ForeignPtr.withForeignPtr`.

Nevertheless, this release has nevertheless seen a considerable amount of
testing and consequently we hope that this should be the last
pre-release before the final release.

In addition to numerous bug fixes, GHC 9.0.1 will bring a number of new
features:

 * A first cut of the new `LinearTypes` [language extension][2], allowing use
   of linear function syntax and linear record fields.

 * A new bignum library (`ghc-bignum`), allowing GHC to be more easily
   used with integer libraries other than GMP.

 * Improvements in code generation, resulting in considerable
   performance improvements in some programs.

 * Improvements in pattern-match checking, allowing more precise
   detection of redundant cases and reduced compilation time.

 * Implementation of the "simplified subsumption" [proposal][3]
   simplifying the type system and paving the way for QuickLook
   impredicativity in GHC 9.2.

 * Implementation of the `QualifiedDo` [extension][4], allowing more
   convenient overloading of `do` syntax.

 * Improvements in compilation time.

And many more. See the [release notes][5] for a full accounting of the
changes in this release.

As always, do test this release and open tickets for whatever issues you
encounter.

[1]: https://gitlab.haskell.org/ghc/ghc/-/wikis/migration/9.0#ghc-prim-07
[2]: https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0111-linear-types.rst
[3]: https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0287-simplify-subsumption.rst
[4]: https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0216-qualified-do.rst 
[5]: https://downloads.haskell.org/ghc/9.0.1-rc1/docs/html/users_guide/9.0.1-notes.html

