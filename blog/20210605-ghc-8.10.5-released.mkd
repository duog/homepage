---
author: Zubin Duggal
title: "GHC 8.10.5 is now available"
date: 2021-06-05
tags: release
---

The GHC team is very pleased to announce the availability of GHC 8.10.5.
Source and binary distributions are available at the [usual
place](https://downloads.haskell.org/ghc/8.10.5/).

This release adds native ARM/Darwin support, as well as bringing performance
improvements and fixing numerous bugs of varying severity present in the 8.10
series:

- First-class support for Apple M1 hardware using GHC's LLVM ARM backend

- Fix a bug resulting in segmentation faults where code may be unloaded
  prematurely when using the parallel garbage collector (#19417) along
  with other bugs in the GC and linker (#19147, #19287)

- Improve code layout fixing certain performance regressions (#18053)
  and other code generation bug fixes (#19645)

- Bug fixes for signal handling when using the pthread itimer implementation.

- Improvements to the specializer and simplifier reducing code size and
  and memory usage (#17151, #18923,#18140, #10421, #18282, #13253).

- Fix a bug where typechecker plugins could be run with an inconsistent
  typechecker environment (#19191).

- Fix a simplifier bug which lead to an exponential blow up and excessive
  memory usage in certain cases 

A complete list of bug fixes and improvements can be found in the release notes: [release
notes](https://downloads.haskell.org/ghc/8.10.5/docs/html/users_guide/8.10.5-notes.html),

As always, feel free to report any issues you encounter via
[gitlab.haskell.org](https://gitlab.haskell.org/ghc/ghc/-/issues/new).
