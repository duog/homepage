---
author: thoughtpolice
title: "GHC Weekly News - 2014/10/31 (Halloween Edition)"
date: 2014-10-31
tags: ghc news
---

Hello \*,

Welcome to the GHC Weekly news. And it's just in time before you go out and eat
lots of candy and food.

  * David Feuer and Joachim Brietner spent some time this past week talking
  about more optimizations for Haskell code for fusing code, and creating
  better consumers and producers. This work includes optimizations of "One shot
  lambdas" (lambdas used at most once) and Call Arity, which was implemented by
  Joachim at Microsoft Research. The thread is here -
  <https://www.haskell.org/pipermail/ghc-devs/2014-October/006901.html>

  The current situation is that Call Arity and One shot analysis tends to have
  good combined results for exploiting more fusion opportunities, but sometimes
  these backfire. As a result, Joachim and David have worked on improving the
  situation - particularly by letting programmers help with a new `oneShot`
  primitive (in Phab:D392 & Phab:D393).

  * Herbert Valerio Riedel opened up a discussion about the origins of code
  contributions. In short, we'd like to have some stronger ground to stand on
  in the face of contributions from contributors - the origin of a change and
  its legal status is a bit nebulous. The thread is here:
  https://www.haskell.org/pipermail/ghc-devs/2014-October/006959.html

  Overall, there's been a lot of separate points made, including CLAs
  (unlikely), "Developer Certificates of Origin" a la the Linux Kernel, and
  reworking how we mark up header files, and keep track of GHC's long history
  of authors.

  If you work on a big project where some of these concerns are real, we'd like to hear what you have to say!

  * Gintautas Milauskas has done some fantastic work for GHC on Windows lately,
  including fixing tests, improving the build, and making things a lot more
  reasonable to use. With his work, we hope GHC 7.10 will finally ship an
  updated MinGW compiler (a long requested feature), and have a lot of good
  fixes for windows. Thank you, Gintautas!

  * And on that note, the call for Windows developers rages on - it looks like
  Gintautaus, Austin, Simon, and others will be meeting to discuss the best way
  to tackle our current woes. Are you a Windows user? Please give us input -
  having input is a crucial part of the decision making process, so let us
  know.

  * Jan Stolarek had a question about core2core - a lot of questions, in fact.
  What's the difference between demand, strictness, and cardinality analylsis?
  Does the demand analyzer change things? And what's going on in some of the
  implementation? A good read if you're interested in deep GHC optimization
  magic: <https://www.haskell.org/pipermail/ghc-devs/2014-October/006968.html>

  * Peter Wortmann has put up the new DWARF generation patches for review, in
  Phab:D396. This is one of the major components we still plan on landing in
  7.10, and with a few weeks to spare, it looks like we can make sure it's
  merged for the STABLE freeze!

There have been a lot of good changes in the tree this past week:

 * Thanks to Michael Orlitzky, we plan on adding doctest examples to more modules in 7.10, and increase that coverage further. This is *really* important work, but very low fruit - thanks a ton Michael!

 * `Data.Bifunctor` is now inside base! (Phab:D336)

 * `atomicModifyIORef'` has been optimized with excellent speedups (as much as 1.7x to 1.4x, depending on the RTS used), thanks to some older work by Patrick Palka (Phab:D315).
 GHC's internals have been reworked to unwire `Integer` from GHC, leading not only to a code cleanup, but laying the foundation for further GMP (and non-GMP!) related `Integer` improvements (Phab:D351).

 * David Feuer and Joachim have been relentless in improving fusion opportunities, including the performance of `take`, `isSuffixOf`, and more prelude improvements, spread over nearly half a dozen patches. And this doesn't even include the work on improving `oneShot` or Call Arity!

 * In a slight change to `base` semantics, David Feuer also finally fixed #9236. This is a change that can expose latent bugs in your program (as it did for Haddock), so be sure to test thoroughly with 7.10 (Phab:D327).

 * GHC now has support for a new `__GLASGOW_HASKELL_TH__` macro, primarily useful for testing bootstrap compilers, or compilers which don't support GHCi.

And there have been many closed tickets: #9549, #9593, #9720, #9031, #8345, #9439, #9435, #8825, #9006, #9417, #9727, #2104, #9676, #2628, #9510, #9740, #9734, #9367, #9726, #7984, #9230, #9681, #9747, and #9236.
