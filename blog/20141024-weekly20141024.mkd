---
author: thoughtpolice
title: "GHC Weekly News - 2014/10/24"
date: 2014-10-24
tags: ghc news
---

Hi \*,

Welcome to the weekly GHC news. This one will be short this week, as the
preceding one occurred only on Monday - but we'll be going with Fridays from
now on, so next week we'll hopefully see a longer list.

 - GHC 7.8.4 tickets have been in waiting, and the RC will be soon after Austin
   finishes some final merges and tests on his branch. **We have not committed a
   time for the release after the RC**, yet we would like people to **please
   seriously test** and immediately report any major showstoppers - or alert us
   of ones we missed.

 - For the [GHC 7.10 release](https://ghc.haskell.org/trac/ghc/wiki/Status/Oct14), 
   one of the major features we planned to try and merge was DWARF debugging
   information. This is actually a small component of larger ongoing work,
   including adding stack traces to Haskell executables. While, unfortunately,
   not all the work can be merged, we talked with Peter, and made a plan: our
   hope is to get Phab:D169 merged, which lays all the groundwork, followed by
   DWARF debugging information in the code generators. This will allow tools
   like `gdb` or other extensible debuggers to analyze C-- IR accurately for
   compiled executables.

   Peter has written up a wiki page, available at SourceNotes, describing the
   design. We hope to land all the core infrastructure in Phab:D169 soon,
   followed by DWARF information for the Native Code Generator, all for 7.10.1

 - This past week, a discussion sort of organically started on the `#ghc` IRC
   channel about the future of the LLVM backend. GHC's backend is buggy, has no
   control over LLVM versions, and breaks frequently with new versions. This all
   significantly impacts users, and relegates the backend to a second class
   citizen. After some discussion, Austin wrote up a [proposal for a improved
   backend](https://ghc.haskell.org/trac/ghc/wiki/ImprovedLLVMBackend), and
   wrangled several other people to help. The current plan is to try an execute
   this by GHC 7.12, with the goal of making the LLVM backend Tier 1 for major
   supported platforms.

 - You may notice https://ghc.haskell.org is now responds slightly faster in
   some cases - we've activated a caching layer (CloudFlare) on the site, so
   hopefully things should be a little more smooth.

Closed tickets this week: #9684, #9692, #9038, #9679, #9537, #1473.
