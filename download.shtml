---
title: Download
---

<h1>Distribution packages</h2>

<div class="body">
    <p>
        GHC is packaged for a number of operating systems and
        distributions. While they may lag behind the latest GHC
        release, advantages such as dependency checking and ease of
        uninstallation mean we recommend using them anyway, unless
        you have a particular need for new features or bug fixes.
    </p>
</div>

<ul>
    <li>
        <a href="distribution_packages.html">Distribution packages</a>
    </li>
</ul>

<h1>Current Stable Releases</h2>

<ul>
    <li><a href="download_ghc_9_2_2.html">9.2.2</a></li>
    <li><a href="download_ghc_9_0_2.html">9.0.2</a></li>
    <li><a href="download_ghc_8_10_7.html">8.10.7</a></li>
</ul>

<h1>Older Releases</h1>

<div class="body">
    <ul>
        <li><a href="download_ghc_9_2_1.html">9.2.1</a></li>
        <li><a href="download_ghc_9_0_1.html">9.0.1</a></li>
        <li><a href="download_ghc_8_10_6.html">8.10.6</a></li>
        <li><a href="download_ghc_8_10_5.html">8.10.5</a></li>
        <li><a href="download_ghc_8_10_4.html">8.10.4</a></li>
        <li><a href="download_ghc_8_10_3.html">8.10.3</a></li>
        <li><a href="download_ghc_8_10_2.html">8.10.2</a></li>
        <li><a href="download_ghc_8_10_1.html">8.10.1</a></li>
        <li><a href="download_ghc_8_8_4.html">8.8.4</a></li>
        <li><a href="download_ghc_8_8_3.html">8.8.3</a></li>
        <li><a href="download_ghc_8_8_2.html">8.8.2</a></li>
        <li><a href="download_ghc_8_8_1.html">8.8.1</a></li>
        <li><a href="download_ghc_8_6_5.html">8.6.5</a></li>
        <li><a href="download_ghc_8_6_4.html">8.6.4</a></li>
        <li><a href="download_ghc_8_6_3.html">8.6.3</a></li>
        <li><a href="download_ghc_8_6_2.html">8.6.2</a></li>
        <li><a href="download_ghc_8_6_1.html">8.6.1</a></li>
        <li><a href="download_ghc_8_4_4.html">8.4.4</a></li>
        <li><a href="download_ghc_8_4_3.html">8.4.3</a></li>
        <li><a href="download_ghc_8_4_2.html">8.4.2</a></li>
        <li><a href="download_ghc_8_4_1.html">8.4.1</a></li>
        <li><a href="download_ghc_8_2_2.html">8.2.2</a></li>
        <li><a href="download_ghc_8_2_1.html">8.2.1</a></li>
        <li><a href="download_ghc_8_0_2.html">8.0.2</a></li>
        <li><a href="download_ghc_8_0_1.html">8.0.1</a></li>
        <li><a href="download_ghc_7_10_3.html">7.10.3</a></li>
        <li><a href="download_ghc_7_10_2.html">7.10.2</a></li>
        <li><a href="download_ghc_7_10_1.html">7.10.1</a></li>
        <li><a href="download_ghc_7_8_4.html">7.8.4</a></li>
        <li><a href="download_ghc_7_8_3.html">7.8.3</a></li>
        <li><a href="download_ghc_7_8_2.html">7.8.2</a></li>
        <li><a href="download_ghc_7_8_1.html">7.8.1</a></li>
        <li><a href="download_ghc_7_6_3.html">7.6.3</a></li>
        <li><a href="download_ghc_7_6_2.html">7.6.2</a></li>
        <li><a href="download_ghc_7_6_1.html">7.6.1</a></li>
        <li><a href="download_ghc_7_4_2.html">7.4.2</a></li>
        <li><a href="download_ghc_7_4_1.html">7.4.1</a></li>
        <li><a href="download_ghc_7_2_2.html">7.2.2</a></li>
        <li><a href="download_ghc_7_2_1.html">7.2.1</a></li>
        <li><a href="download_ghc_7_0_4.html">7.0.4</a></li>
        <li><a href="download_ghc_7_0_3.html">7.0.3</a></li>
        <li><a href="download_ghc_7_0_2.html">7.0.2</a></li>
        <li><a href="download_ghc_7_0_1.html">7.0.1</a></li>
        <li><a href="download_ghc_6_12_3.html">6.12.3</a></li>
        <li><a href="download_ghc_6_12_2.html">6.12.2</a></li>
        <li><a href="download_ghc_6_12_1.html">6.12.1</a></li>
        <li><a href="download_ghc_6_10_4.html">6.10.4</a></li>
        <li><a href="download_ghc_6_10_3.html">6.10.3</a></li>
        <li><a href="download_ghc_6_10_2.html">6.10.2</a></li>
        <li><a href="download_ghc_6_10_1.html">6.10.1</a></li>
        <li><a href="download_ghc_683.html">6.8.3</a></li>
        <li><a href="download_ghc_682.html">6.8.2</a></li>
        <li><a href="download_ghc_681.html">6.8.1</a></li>
        <li><a href="download_ghc_661.html">6.6.1</a></li>
        <li><a href="download_ghc_66.html">6.6</a></li>
        <li><a href="download_ghc_642.html">6.4.2</a></li>
        <li><a href="download_ghc_641.html">6.4.1</a></li>
        <li><a href="download_ghc_64.html">6.4</a></li>
        <li><a href="download_ghc_622.html">6.2.2</a></li>
        <li><a href="download_ghc_621.html">6.2.1</a></li>
        <li><a href="download_ghc_62.html">6.2</a></li>
        <li><a href="download_ghc_601.html">6.0.1</a></li>
        <li><a href="download_ghc_600.html">6.0</a></li>
        <li><a href="download_ghc_5043.html">5.04.3</a></li>
        <li><a href="download_ghc_5042.html">5.04.2</a></li>
        <li><a href="download_ghc_5041.html">5.04.1</a></li>
        <li><a href="download_ghc_504.html">5.04</a></li>
        <li><a href="download_ghc_502.html">5.02.x</a></li>
        <li><a href="download_ghc_500.html">5.00.x</a></li>
        <li><a href="download_ghc_408.html">4.08.x</a></li>
        <li><a href="download_ghc_406.html">4.06</a></li>
        <li><a href="download_ghc_302.html">3.02</a></li>
        <li><a href="download_ghc_210.html">2.10</a></li>
        <li><a href="download_ghc_029.html">0.29</a></li>
    </ul>
</div>
